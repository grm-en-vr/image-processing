{
    "xray":{
		"sid": 400,//Distance of ray source (focal point) [default: 400mm]"
		"npixel_x":256,// number of pixels along X of the 2D DRR image
		"npixel_y":256,// number of pixels along Y of the 2D DRR image
		"res_x":1,// pixel spacing along X of the 2D DRR image [mm]
		"res_y":1 // pixel spacing along Y of the 2D DRR image [mm]
    },
    "transformation":{
		"tx_min":50.0,//Translation parameter of the camera
		"tx_max":100.0, 
		"tx_delta":10, 
		"ty_min":1,
		"ty_max":3, 
		"ty_delta":1, 
		"tz_min":1, 
		"tz_max":3, 
		"tz_delta":1,
		
		"rx_min":0, //Rotation around x,y,z axis in degrees 
		"rx_max":90, 
		"rx_delta":10, 
		"ry_min":0, 
		"ry_max":90, 
		"ry_delta":10, 
		"rz_min":0, 
		"rz_max":90, 
		"rz_delta":10
	}
}
